/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olamundojavafcx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author maria
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    //colocar o nome dos controles
    private Label lblMensagem;
    //digita o nome do botao
    // adiciono importacao pro botao
    private Button btnClick;
    
    @FXML
    //programando o evento do botao
    //primeiro eu modifiquei no botao da janelinha no action e coloquei o mesmo
    //nome dele aqui "clicouBotao"
    private void clicouBotao(ActionEvent event) {
        //digito o comando swing
        //foi digitado 
        lblMensagem.setText("Ola Mundo");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
